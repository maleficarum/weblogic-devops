= Construct Maven Plugin

```bash
cd $ORACLE_HOME/oracle_common/plugins/maven/com/oracle/maven/oracle-maven-sync/cd 12.1.3/
  
mvn install:install-file -DpomFile=oracle-maven-sync-12.1.3.pom -Dfile=oracle-maven-sync-12.1.3.jar 
mvn com.oracle.maven:oracle-maven-sync:push -Doracle-maven-sync.oracleHome=/usr/local/software/Oracle/Middleware/Oracle_Home
mvn -DgroupId=com.oracle.weblogic -DartifactId=weblogic-maven-plugin -Dversion=12.1.3-0-0
mvn help:describe -DartifactId=weblogic-maven-plugin -Dversion=12.1.3-0-0
```

== Maven goal

```
com.oracle.weblogic:weblogic-maven-plugin:deploy
```
